# lib/csv_reform/payload.rb

# Payload
module PayloadModule
  def export
    raise 'error: invalid CSV separator' unless CsvReform.validate_csv(self.source)
    raise 'error: header validation' unless validate_header
    

    header = YAML.load_file self.schema

    CSV.open(self.target, 'w', col_sep: ';') do |csv|
      csv << header.keys

      CSV.foreach(self.source, headers: true, col_sep: ';') do |i|
        row = []
        header.each do |column|
          row << i[column.last]
        end

        csv << row
      end
    end
  end

  def generate_target_filename(filename:, filetype: nil) 
    if filetype.nil?
      filetype = File.basename(filename).split('.').last
    end

    "#{File.basename(filename).split('.').first}.#{filetype}"
  end

  def validate_header
    source = CSV.read(self.source, headers:true, col_sep: ';', return_headers:true)
    schema = YAML.load_file self.schema

    schema.values.each do |i|
      return false unless source.headers.include? i
    end
  end
end

# Payload
class Payload
  include PayloadModule

  def initialize(source:, target: nil, schema: nil)
    @source = source

    # set target
    if target.nil?
      @target = generate_target_filename(filename: source)
    else
      @target = target
    end

    # set schema 
    if schema.nil?
      @schema = source + '.yml'
    else
      @schema = schema
    end
  end

  attr_reader :source
  attr_reader :target
  attr_reader :schema
end
