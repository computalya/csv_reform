require 'csv'

module CsvReform
  def self.validate_csv(file)
    separator = CsvReform.validate_separator(file)
    # integer   = Reform.validate_integer(file)

    # separator && integer
  end

  def self.validate_separator(file)
    separator_set = Set.new

    # count separator
    CSV.foreach(file, col_sep: ';') do |i|
      separator_set.add i.compact.size
    end

    separator_set.size == 1
  end

  def self.validate_integer(file)
    CSV.read(file, col_sep: ';')[1 .. -1] do |i|
      i.all? { |i| return false unless i.is_a? Integer }
    end
  end
end
