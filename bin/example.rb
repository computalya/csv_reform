#!/usr/bin/env ruby

# bin/example.rb
require 'bundler/setup'
require 'csv_reform'

source = 'data/incoming/example.csv'
target = 'data/output/example.new.csv'

x = Payload.new(source: source, target: target)

x.export

puts "#{x.target} has been created"
