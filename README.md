# CsvReform

Reformat CSV file based on a YML file

- renames column headers
- creates new file with predefined headers order

## Installation

```bash
cd /tmp
git clone git@gitlab.com:computalya/csv_reform.git

gem install csv_reform/pkg/csv_reform-0.2.2.gem
```

## Payload

`example.csv`

```csv
FirstName;Lastname;PhoneNr
luke;skywalker;1
darth;vader;2
obiwan;kenobi;3
```

`example.csv.yml`

```yml
---
name: 'FirstName'
lastname: 'Lastname'
phone: 'PhoneNr'
```

`example.new.csv`

```csv
name;lastname;phone
luke;skywalker;1
darth;vader;2
obiwan;kenobi;3
```

### Example 1

generates example.csv in the current folder

```ruby
pry
require 'csv_reform'

source = 'data/incoming/example.csv'

x = Payload.new(source: source)
x.export
```

### Example 2

generates example.csv in the target directory

```ruby
pry
require 'csv_reform'

source = 'data/incoming/example.csv'
target = 'data/output/example.new.csv'

x = Payload.new(source: source, target: target)
x.export
```

### Example 3

set schema file. Default is in the current directory source_file_name.csv.yml

```ruby
pry
require 'csv_reform'

source = 'data/incoming/example.csv'
schema = 'data/incoming/example.csv.yml'
target = 'data/output/example.new.csv'

x = Payload.new(source: source, target: target, schema: schema)
x.export
```

## Show parameters

```ruby
pry
require 'csv_reform'

source = 'data/incoming/example.csv'
schema = 'data/incoming/example.csv.yml'
target = 'data/output/example.new.csv'

x = Payload.new(source: source, target: target, schema: schema)

x.source
# => "data/incoming/example.csv"

x.target
# => "data/output/example.new.csv"

x.schema
# => "data/incoming/example.csv.yml"
```

