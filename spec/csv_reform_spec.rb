RSpec.describe CsvReform do
  let(:fixture_path) { 'spec/fixtures/files' }

  it 'has a version number' do
    expect(CsvReform::VERSION).not_to be nil
  end

  context 'fixtures' do
    # directory should exists
    it { expect(File).to be_directory fixture_path }

    # file should exists
    it { expect(File).to exist("#{fixture_path}/file-exists.csv") }

    # compare file content
    it 'reads sample file' do
      file = "#{fixture_path}/file-exists.csv"
      data = 'hello'
      fileContent = File.open(file, 'r')
      stringFile = fileContent.read

      expect(stringFile).to eq(data)
    end
  end
end
