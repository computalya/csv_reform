# payload_spec.rb
RSpec.describe Payload do
  let(:fixture_path) { 'spec/fixtures/files' }

  context 'has methods' do
    let(:payload) { Payload.new(source: 'abc.txt', target: 'abc.txt') }
    it ':source' do
      expect(payload.methods.include? :source).to be_truthy
    end

    it ':target' do
      expect(payload.methods.include? :target).to be_truthy
    end

    it ':schema' do
      expect(payload.methods.include? :schema).to be_truthy
    end

    it ':export' do
      expect(payload.methods.include? :export).to be_truthy
    end

    it ':validate_header' do
      expect(payload.methods.include? :validate_header).to be_truthy
    end

    it ':generate_target_filename' do
      expect(payload.methods.include? :generate_target_filename).to be_truthy
    end
  end

  context 'validate_header' do
    it 'invalid when column name is missing in CSV' do
      source = "#{fixture_path}/missing-header.csv"
      schema = "#{fixture_path}/missing-header.csv.yml"

      x = Payload.new(source: source)
      expect(x.validate_header).to be_falsey
    end

    it 'valid when CSV includes column names' do
      source = "#{fixture_path}/valid-header.csv"
      schema = "#{fixture_path}/valid-header.csv.yml"

      x = Payload.new(source: source)
      expect(x.validate_header).to be_truthy
    end

    it 'valid when column names are not sorted' do
      source = "#{fixture_path}/valid-header-unsorted.csv"
      schema = "#{fixture_path}/valid-header-unsorted.csv.yml"

      x = Payload.new(source: source)
      expect(x.validate_header).to be_truthy
    end
    
    # ctrlm.csv.yml
    it 'valid when ^M line endings are used' do
      source = "#{fixture_path}/ctrlm.csv"
      schema = "#{fixture_path}/ctrlm.csv.yml"

      x = Payload.new(source: source)
      expect(x.validate_header).to be_truthy
    end

  end

end
