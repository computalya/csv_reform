# spec/validate_csv_spec.rb
RSpec.describe CsvReform do
  let(:fixture_path) { 'spec/fixtures/files' }

  context 'fixtures' do
    # file should exists
    it { expect(File).to exist("#{fixture_path}/valid.csv") }
  end

  context :validate_csv do
    it { expect(CsvReform).to_not respond_to(:validate_csv).with(0).argument }
    it { expect(CsvReform).to respond_to(:validate_csv).with(1).argument }
  end

  context 'valid file' do
    it 'validate' do
      file = "#{fixture_path}/valid.csv"
      expect(CsvReform.validate_csv(file)).to be_truthy
    end

    it 'validate UTF-8 characters' do
      file = "#{fixture_path}/valid-utf8.csv"
      expect(CsvReform.validate_csv(file)).to be_truthy
    end
  end

  context 'invalid file' do
    it 'wrong number of columns(2 instead of 3)' do
      file = "#{fixture_path}/invalid1.csv"
      expect(CsvReform.validate_csv(file)).to be_falsey
    end

    it 'wrong number of columns(4 instead of 3)' do
      file = "#{fixture_path}/invalid2.csv"
      expect(CsvReform.validate_csv(file)).to be_falsey
    end

    it 'nil in header' do
      file = "#{fixture_path}/invalid3.csv"
      expect(CsvReform.validate_csv(file)).to be_falsey
    end

    it 'nil in row' do
      file = "#{fixture_path}/invalid4.csv"
      expect(CsvReform.validate_csv(file)).to be_falsey
    end
  end
end
